import logging

import ftrack_api

import os
import sys
import pprint

import ftrack_connect.application

def debug(message):
    f = open("C:\\Users\\aaron\\Desktop\\output.txt", "a")
    f.write(pprint.pformat(message))
    f.close()

class ApplicationStore(ftrack_connect.application.ApplicationStore):
    '''Store used to find and keep track of available applications.'''

    def _discoverApplications(self):
        '''Return a list of applications that can be launched from this host.'''
        applications = []

        if sys.platform == 'darwin':
            prefix = ['/', 'Applications']

            applications.extend(self._searchFilesystem(
                expression=prefix + [
                    'Fusion*', 'Fusion.app'
                ],
                label='Fusion {version}',
                applicationIdentifier='fusion_{version}'
            ))

        elif sys.platform == 'win32':
            prefix = ['C:\\', 'Program Files.*']

            applications.extend(self._searchFilesystem(
                expression=(
                    prefix +
                    ['Blackmagic Design', 'Fusion*', 'Fusion.exe']
                ),
                label='Fusion {version}',
                applicationIdentifier='fusion_{version}'
            ))

        self.logger.debug(
            'Discovered applications:\n{0}'.format(
                pprint.pformat(applications)
            )
        )

        return applications

class FusionAction(object):
    '''Launch Fusion action.'''

    # Unique action identifier.
    identifier = 'fusion-launch-action'

    def __init__(self, applicationStore, launcher):
        '''Initialise action with *applicationStore*.'''
        super(FusionAction, self).__init__()

        self.logger = logging.getLogger(
            __name__ + '.' + self.__class__.__name__
        )

        self.applicationStore = applicationStore
        self.launcher = launcher

        if self.identifier is None:
            raise ValueError('The action must be given an identifier.')

    def register(self, session):
        '''Register action.'''
        session.event_hub.subscribe(
            'topic=ftrack.action.discover',
            self.discover
        )

        session.event_hub.subscribe(
            'topic=ftrack.action.launch and data.actionIdentifier={0}'.format(
                self.identifier
            ),
            self.launch
        )

        self.session = session

    def is_component(self, selection):
        if (
            len(selection) != 1 or
            selection[0]['entityType'] != 'Component'
        ):
            return False

        return True
        
    def discover(self, event):
        '''Return action based on *event*.'''

        launchArguments = []

        selection = event['data'].get('selection', [])

        if self.is_component(selection):
            component = self.session.get('Component', selection[0]['entityId'])
            
            if component is not None:
                location = self.session.pick_location()
                url = location.get_filesystem_path(component)
                launchArguments.append(url)

        items = []
        applications = self.applicationStore.applications
        applications = sorted(
            applications, key=lambda application: application['label']
        )

        for application in applications:
            applicationIdentifier = application['identifier']
            label = application['label']
            items.append({
                'actionIdentifier': self.identifier,
                'label': label,
                'icon': 'https://www.lunadigital.tv/ftrack/application_icons/fusion.png',
                'applicationIdentifier': applicationIdentifier,
                'launchArguments': launchArguments
            })

        return {
            'items': items
        }

    def launch(self, event):
        '''Callback method for Fusion action.'''
        applicationIdentifier = (
            event['data']['applicationIdentifier']
        )

        context = event['data'].copy()

        return self.launcher.launch(
            applicationIdentifier, context
        )

class ApplicationLauncher(ftrack_connect.application.ApplicationLauncher):
    '''Custom launcher to modify environment before launch.'''
    
    def _getApplicationLaunchCommand(self, application, context=None):
        command = super(ApplicationLauncher, self)._getApplicationLaunchCommand(application, context)
        command.extend(context.get('launchArguments'))
        return command

def register(session, **kw):
    '''Register action in Connect.'''

    # Validate that session is an instance of ftrack_api.Session. If not, assume
    # that register is being called from an old or incompatible API and return
    # without doing anything.
    if not isinstance(session, ftrack_api.Session):
        return

    applicationStore = ApplicationStore()

    launcher = ApplicationLauncher(
        applicationStore
    )

    action = FusionAction(applicationStore, launcher)
    action.register(session)