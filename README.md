# ftrack connect fusion #

Blackmagic Fusion integration with ftrack.

### Documentation ###

To get started, obtain a copy of the source by cloning the public repository:

    git clone https://bitbucket.org/luna-digital/ftrack-connect-fusion.git

Then you can build and install the package into your current Python site-packages folder:

    python setup.py build_plugin

The resulting plugin will then be available under the build folder. Copy or symlink the resulting plugin folder in your FTRACK_CONNECT_PLUGIN_PATH.

### Copyright and license ###

TBD

### Functionality Status ###

The latest stable version of this plugin has the following features:

* Can detect and open a Fusion session from ftrack Connect
* Technically should be able to also open a component file, but hasn't been tested yet.